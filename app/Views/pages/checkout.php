<!-- breadcrumb-section -->
<div class="breadcrumb-section breadcrumb-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-center">
                <div class="breadcrumb-text">
                    <p>Premium and Original</p>
                    <h1>Check Out Product</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end breadcrumb section -->

<!-- check out section -->
<div class="checkout-section mt-150 mb-150">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="checkout-accordion-wrap">
                    <div class="accordion" id="accordionExample">
                        <div class="card single-accordion">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Billing Address
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="billing-address-form">
                                        <form action="index.html">
                                            <p><input type="text" placeholder="Nama"></p>
                                            <p><input type="email" placeholder="Email"></p>
                                            <p><input type="tel" placeholder="No.Telp"></p>
                                            <p><input type="text" placeholder="Detail Lainnya (Cth: Nama Panggilan)"></p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card single-accordion">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Shipping Address
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="shipping-address-form">
                                        <form action="#">
                                            <p><input type="text" placeholder="Provinsi"></p>
                                            <p><input type="text" placeholder="Kota/Kabupaten"></p>
                                            <p><input type="text" placeholder="Kecamatan"></p>
                                            <p><input type="text" placeholder="Kode Pos"></p>
                                            <p><input type="text" placeholder="Nama Jalan"></p>
                                            <p><input type="text" placeholder="Detail Lainnya (Cth: Nama Gedung, No.Rumah)"></p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card single-accordion">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Card Details
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="card-details">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Transfer Bank
                                        </button>
                                    </div>

                                    <div class="card-E_wallet">
                                        <a href="#"><button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                E-Wallet
                                            </button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-4">
                <div class="order-details-wrap">
                    <table class="order-details">
                        <thead>
                            <tr>
                                <th>Your order Details</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody class="order-details-body">
                            <tr>
                                <td>Product</td>
                                <td>Total</td>
                            </tr>
                            <tr>
                                <td>Resistor 1K</td>
                                <td>Rp100</td>
                            </tr>
                            <tr>
                                <td>Kapasitor Polyster</td>
                                <td>Rp1.000</td>
                            </tr>
                            <tr>
                                <td>JFET</td>
                                <td>Rp17.000</td>
                            </tr>
                        </tbody>
                        <tbody class="checkout-details">
                            <tr>
                                <td>Subtotal</td>
                                <td>RP18.100</td>
                            </tr>
                            <tr>
                                <td>Shipping</td>
                                <td>Rp13.000</td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td>Rp31.100</td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="#" class="boxed-btn">Place Order</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end check out section -->

<!-- logo carousel -->
<div class="logo-carousel-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="logo-carousel-inner">
                    <div class="single-logo-item">
                        <img src="/assets/img/company-logos/IPB.png" alt="">
                    </div>
                    <div class="single-logo-Tekom">
                        <img src="/assets/img/company-logos/Tekom.png" alt="">
                    </div>
                    <div class="single-logo-Olivia">
                        <img src="/assets/img/company-logos/Olivia.png" alt="">
                    </div>
                    <div class="single-logo-SV">
                        <img src="/assets/img/company-logos/SV-1.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end logo carousel -->